<?php
    session_start();
    
    ini_set('error_reporting', E_ALL);
    ini_set('session.cookie_httponly', true);
    ini_set('session.gc_probability', 1);
    ini_set('date.timezone', 'US/Central');
    
    function open_connection($type = 'zebra',$database = 'new-lister'){
        $open = new connection($type,$database);
        return $open->connect();
    }
    
    class connection{
        public $connection;
        private $hostname = 'localhost';
        private $username = '';
        private $userpass = '';
        private $database;
        
        public function __construct($type,$database){
            $this->database = $database;
            switch($type){
                case 'mysqli':
                    $this->connection = new mysqli($this->hostname,$this->username,$this->userpass,$this->database);
                    break;
                case 'zebra':
                    require_once('Zebra_Database/Zebra_Database.php');
                    $this->connection = new Zebra_Database();
                    $this->connection->debug = false;
                    /*$this->connection->memcache_host = 'localhost';
                    $this->connection->memcache_port = 11211;
                    $this->connection->memcache_compressed = false;
                    $this->connection->memcache_method = 'memcache';*/
                    $this->connection->connect($this->hostname,$this->username,$this->userpass,$this->database);
                    $this->connection->set_charset();
                    break;
            }
        }
        
        public function connect(){
            return $this->connection;
        }
    }
