<?php echo 'welcome '; 
     echo $_SESSION['user_name']; ?>
<!DOCTYPE html>
<html>
<head>
<title>Welcome to Collections Empire!</title>
<link href="./views/styles/css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
<link href='http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
<link href='./views/styles/main.css' rel='stylesheet'>

</head>

<body>
<div class="content title">
	<h1>Collections Empire <span class="demo"> Listings </h1>
	<span class="back-to-article"><a href="./index.php?logout">logout<i class="fa fa-angle-right"></i></a></span>
</div>
<div class="content white">
	<nav class="navbar navbar-default" role="navigation">
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="#">Manage Account</a>
	    </div>
	    <!--/.navbar-header-->
	
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <ul class="nav navbar-nav">
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Submissions<b class="caret"></b></a>
		            <ul class="dropdown-menu">
			            <li><a href="views/upload/upload.html">Upload Pictures</a></li>
                                    <li><a href="views/crop/index.html">Crop Photos</a></li>
			            <li><a href="./views/submit/submit.html">Edit Jewelery Descriptions</a></li>
                                    <li><a href="./views/submit/submitphones.html">Edit Phone Descriptions</a></li>
                                    <li><a href="./views/movies/index.html">Edit DVD Descriptions</a></li>
			            <li><a href="#">Submit To Storefront's</a></li>
		            </ul>
		        </li>
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Store <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-6">
					            <ul class="multi-column-dropdown">
						            <li><a href="#">Get Sold Listings ></li>
						            <li><a href="#">Magento</a></li>
					            </ul>
				            </div>
		            </ul>
		        </li>
                       <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Tracking <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-6">
					            <ul class="multi-column-dropdown">
						            <li><a href="#">Get Sold Listings ></li>
						            <li><a href="#">Another action</a></li>
						            <li><a href="#">Something else here that's extra long so we can see how it looks</a></li>
						            <li class="divider"></li>
						            <li><a href="#">Separated link</a></li>
						            <li class="divider"></li>
						            <li><a href="#">One more separated link</a></li>
					            </ul>
				            </div>
		            </ul>
		        </li>
		        <li><a href="http://www.ebay.com/">Login to eBay</a></li>
	        </ul>
	    </div>
	    <!--/.navbar-collapse-->
	</nav>
	<!--/.navbar-->
	
</div>
<div id="main-content" class="content">
</div>

<script type="text/javascript" src="./libraries/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="./views/styles/js/bootstrap-3.1.1.min.js"></script>
<script type="text/javascript" src="./views/styles/js/general.js" ></script>
<script type = "text/javascript" src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

</body>
</html>
