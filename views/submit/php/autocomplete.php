<?php
 
// Data could be pulled from a DB or other source
$cities = array(
	'model'=>'ZTE Avid 4G', 'internalmemory='>'4 GB (2.3 GB user available), 512 MB RAM', 'ExternalMemory'=>'microSD, up to 32 GB', 'bands'=>'2g:CDMA 800/1700/1900/2100 3g:CDMA2000 1xEV-DO',
        'model'=>'Samsung Galaxy Mega 2', 'internalmemory'=>'16 GB, 1.5 GB RAM', 'ExternalMemory'=>'microSD, up to 64 GB', 'bands'=>'2g:GSM 850/900/1800/1900 3g:CDMA2000 1xEV-DOLTE 1700 / 2100',
 );
// Cleaning up the term
$term = trim(strip_tags($_GET['term']));
 
// Rudimentary search
$matches = array();
foreach($models as $model){
	if(stripos($model['model'], $term) !== false){
		// Add the necessary "value" and "label" fields and append to result set
		$model['value'] = $model['model'];
		$model['label'] = "{$model['model']}, {$model['internalmemory']} {$model['ExternalMemory']} {$model['bands']}";
		$matches[] = $model;
	}
}
 
// Truncate, encode and return the result
print json_encode($matches);