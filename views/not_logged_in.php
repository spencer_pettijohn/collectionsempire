<?php
// show potential errors / feedback (from login object)
if (isset($login)) {
    if ($login->errors) {
        foreach ($login->errors as $error) {
            echo $error;
        }
    }
    if ($login->messages) {
        foreach ($login->messages as $message) {
            echo $message;
        }
    }
}
?>

<html>
   <head>
    <title>Submit Listings</title>
        <link rel="stylesheet" href="./libraries/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./libraries/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="http://jqueryvalidation.org/files/demo/site-demos.css">
        <link rel="stylesheet" href="./views/styles/login.css"> 
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    </head>
<body>
    <div id="banner"></div>
  <div id="login">
    <form method="post" action="index.php" name="loginform">
           </br>
        <input id="login_input_username" class="login_input form-group" type="text" placeholder="username" name="user_name" required />
          </br>
        <input id="login_input_password" class="login_input form-group" type="password" name="user_password" placeholder="password" autocomplete="off" required />
          </br>
        <input type="submit" class="btn-primary" name="login" value="Log in" />
    </form>
   </div>
</body>