<?php

function makearray() {
    $array = array();
    foreach (glob("*.jpg") as $filename) {
        $x = "$filename" . "\n";
        $output = preg_replace( '/[^0-9 _ ]/', '', $x );
        $out = str_replace(' ', '', $output);
        $array += [$out => $filename];
    }
   $fp = fopen('img.json', 'w');
   fwrite($fp, json_encode($array));
   fclose($fp);

}

makearray();
?>
